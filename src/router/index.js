import { createRouter, createWebHistory } from 'vue-router'
import AppHomePage from '../views/AppHomePage'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: AppHomePage
  },
  {
    path: '/cart',
    name: 'Cart',
    component: () => import('../views/AppCartPage')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
