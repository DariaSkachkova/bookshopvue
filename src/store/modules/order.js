export default {
    namespaced: true,
    state: {
        price: 0
    },
    getters: {
        price(state) {
            return state.price
        }
    },
    mutations: {
        setPrice(state, payload) {
            state.price = payload
        },
        clearPrice(state) {
            state.price = 0
        }
    },
    actions: {
        clearPriceByTimeout({ commit }, timeout) {
            setTimeout(() => commit('clearPrice'), timeout)
        }
    }
}