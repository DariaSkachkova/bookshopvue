export default {
    namespaced: true,
    state: {
        booksInCart: [],
    },
    getters: {
        booksInCart(state) {
            return state.booksInCart
        },
        booksInCartCount(state) {
            return state.booksInCart.length
        },
        booksInCartTotalPrice(state) {
            return state.booksInCart.reduce((accum, item) => {
                return accum += item.price
            }, 0)
        }
    },
    mutations: {
        addBookIntoCart(state, payload) {
            state.booksInCart.push(payload)
        },
        clearCart(state) {
            state.booksInCart = []
        }
    },
    actions: {

    }
}