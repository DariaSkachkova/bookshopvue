export default {
    namespaced: true,
    state: {
        booksInfo: []
    },
    getters: {
        booksInfo(state) {
            return state.booksInfo
        },
        getBookByIndex: (state) => index => {
            return state.booksInfo[index]
        }
    },
    mutations: {
        setSearchInfo(state, payload) {
            state.booksInfo = payload.items.map(item => {
                let price

                if ("listPrice" in item.saleInfo) {
                    if ("amount" in item.saleInfo.listPrice) {
                        price = item.saleInfo.listPrice.amount
                    }
                } else {
                    price = 0
                }

                return {
                    imageUrl: item.volumeInfo.imageLinks.smallThumbnail ?? '',
                    title: item.volumeInfo.title ?? '',
                    description: item.volumeInfo.description ?? '',
                    authors: item.volumeInfo.authors ?? [],
                    publishedDate: item.volumeInfo.publishedDate ?? '',
                    price
                }
            })
        },

    },
    actions: {
        async fetchSearchInfo({commit}, payload) {
            const response = await fetch(`https://www.googleapis.com/books/v1/volumes?q=${payload}`)

            commit('setSearchInfo', await response.json())
        }
    }
}