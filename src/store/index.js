import { createStore } from 'vuex'
import gallery from './modules/gallery'
import cart from './modules/cart'
import order from './modules/order'

export default createStore({
  modules: {
    gallery,
    cart,
    order
  }
})
